package ru.t1.chubarov.tm.repository;

import ru.t1.chubarov.tm.api.repository.IUserRepository;
import ru.t1.chubarov.tm.model.User;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByLogin(final String login) {
        return records
                .stream()
                .filter(m -> login.equals(m.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User findByEmail(final String email) {
        return records
                .stream()
                .filter(m -> email.equals(m.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public boolean isLoginExist(final String login) {
        return records
                .stream()
                .anyMatch(m -> login.equals(m.getLogin()));
    }

    @Override
    public boolean isEmailExist(final String email) {
        return records.
                stream()
                .anyMatch(m -> email.equals(m.getEmail()));
    }

}
