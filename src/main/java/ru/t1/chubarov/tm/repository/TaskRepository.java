package ru.t1.chubarov.tm.repository;

import ru.t1.chubarov.tm.api.repository.ITaskRepository;
import ru.t1.chubarov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractUserOwnerRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        return records
                .stream()
                .filter(m -> userId.equals(m.getUserid()))
                .filter(m -> projectId.equals(m.getProjectId()))
                .collect(Collectors.toList());
    }



}
