package ru.t1.chubarov.tm.command.task;

import ru.t1.chubarov.tm.enumerated.TaskSort;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.model.Task;
import ru.t1.chubarov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskShowCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(TaskSort.values()));
        final String sortType = TerminalUtil.nextLine();
        final TaskSort sort = TaskSort.toSort(sortType);
        final String userId = getUserId();
        final List<Task> tasks = getTaskService().findAll(userId, sort.getComparator());
        int index = 1;
        for (final Task task : tasks) {
            if (task == null) return;
            System.out.println(index + ". " + task.getName());
            index++;
        }
    }

    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public String getDescription() {
        return "Show list task.";
    }

}
