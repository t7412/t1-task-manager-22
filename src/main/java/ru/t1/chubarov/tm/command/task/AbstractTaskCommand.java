package ru.t1.chubarov.tm.command.task;

import ru.t1.chubarov.tm.api.service.ITaskService;
import ru.t1.chubarov.tm.command.AbstractCommand;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.exception.field.UserIdEmptyException;
import ru.t1.chubarov.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    public String getArgument() {
        return null;
    }

    public Role[] getRoles() {
        return Role.values();
    }

    public void renderTask(final List<Task> tasks) {
        System.out.println("[TASK LIST]");
        int index = 1;
        for (final Task task : tasks) {
            if (task == null) return;
            System.out.println(index + ". " + task.getName());
            System.out.println("    id: " + task.getId());
            System.out.println("    status: " + task.getStatus());
            System.out.println("    desc: " + task.getDescription());
            index++;
        }
    }

    public void showTask(final String userId, final Task task) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

}
