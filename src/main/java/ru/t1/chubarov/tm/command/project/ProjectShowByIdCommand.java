package ru.t1.chubarov.tm.command.project;

import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.model.Project;
import ru.t1.chubarov.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @Override
    public void execute() throws AbstractException {
        final String userId = getUserId();
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        final Project project = getProjectService().findOneById(userId, id);
        showProject(project);
    }

    @Override
    public String getName() {
        return "project-show-by-id";
    }

    @Override
    public String getDescription() {
        return "Display project by Id.";
    }

}
