package ru.t1.chubarov.tm.command.task;

import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.util.TerminalUtil;

public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[COMPLETE TASK BY ID");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        getTaskService().changeTaskStatusById(userId, id, Status.COMPLETED);
    }

    @Override
    public String getName() {
        return "task-complete-by-id";
    }

    @Override
    public String getDescription() {
        return "Update task status to [Completed] by id.";
    }

}
