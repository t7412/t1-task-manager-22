package ru.t1.chubarov.tm.command.project;

import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[START PROJECT BY ID");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        getProjectService().changeProjectStatusById(userId, id, Status.IN_PROGRESS);
    }

    @Override
    public String getName() {
        return "project-start-by-id";
    }

    @Override
    public String getDescription() {
        return "Update project status to [In progress] by id.";
    }

}
