package ru.t1.chubarov.tm.command.project;

import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.util.TerminalUtil;

public final class ProjectTaskBindToProjectCommand extends AbstractProjectCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[BIND TASK TO PROJECT");
        System.out.println("ENTER PROJECT ID: ");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID: ");
        final String taskId = TerminalUtil.nextLine();
        final String userId = getUserId();
        getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
    }

    @Override
    public String getName() {
        return "task-bind-to-project";
    }

    @Override
    public String getDescription() {
        return "Bind task to project bi id.";
    }

}
