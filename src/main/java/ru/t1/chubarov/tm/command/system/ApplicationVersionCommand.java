package ru.t1.chubarov.tm.command.system;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.22.0");
    }

    @Override
    public String getName() {
        return "version";
    }

    @Override
    public String getDescription() {
        return "Show program version.";
    }

    @Override
    public String getArgument() {
        return "-v";
    }

}
