package ru.t1.chubarov.tm.command.user;

import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.model.User;
import ru.t1.chubarov.tm.util.TerminalUtil;

public final class UserRemoveCommand extends AbstractUserCommand {

    private final String NAME = "user-remove";
    private final String DESCRIPTION = "Remove user.";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER REMOVE]");
        System.out.println("[ENTER LOGIN]");
        final String login = TerminalUtil.nextLine();
        getUserService().removeByLogin(login);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
