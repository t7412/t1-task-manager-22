package ru.t1.chubarov.tm.command.task;

import ru.t1.chubarov.tm.exception.AbstractException;

public final class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[TASK CLEAR]");
        final String userId = getUserId();
        getTaskService().removeAll(userId);
    }

    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all task.";
    }

}
