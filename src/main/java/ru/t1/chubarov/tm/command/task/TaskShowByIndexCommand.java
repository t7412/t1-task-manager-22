package ru.t1.chubarov.tm.command.task;

import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.exception.field.UserIdEmptyException;
import ru.t1.chubarov.tm.exception.user.AccessDeniedException;
import ru.t1.chubarov.tm.model.Task;
import ru.t1.chubarov.tm.util.TerminalUtil;

import java.util.List;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws AbstractException {
        final String userId = getUserId();
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("[ENTER TASK INDEX:]");
//        final String projectid = TerminalUtil.nextLine();
//        final List<Task> tasks = getTaskService().findAllByProjectId(userId, projectid);
        final Integer index = TerminalUtil.nextNumber()-1;
        final Task tasks = getTaskService().findOneByIndex(userId, index);
        showTask(userId,tasks);
    }

    @Override
    public String getName() {
        return "task-show-by-index";
    }

    @Override
    public String getDescription() {
        return "Display task by index.";
    }

}
