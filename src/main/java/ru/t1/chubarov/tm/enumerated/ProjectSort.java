package ru.t1.chubarov.tm.enumerated;

import ru.t1.chubarov.tm.comparator.CreatedComparator;
import ru.t1.chubarov.tm.comparator.NameComparator;
import ru.t1.chubarov.tm.comparator.StatusComparator;
import ru.t1.chubarov.tm.model.Project;

import java.util.Comparator;

public enum ProjectSort {

    BY_NAME("Sort by name", NameComparator.INSTANCE::compare),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE::compare),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE::compare);

    public static ProjectSort toSort(final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final ProjectSort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

    private final String name;
    private final Comparator<Project> comparator;

    ProjectSort(final String name, final Comparator<Project> comparator) {
        this.name = name;
        this.comparator = comparator;
    }

    public String getName() {
        return name;
    }

    public Comparator<Project> getComparator() {
        return comparator;
    }

}
