package ru.t1.chubarov.tm.api.service;

import ru.t1.chubarov.tm.api.repository.IRepository;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IService<M extends AbstractModel> extends IRepository<M> {

    M add(M model) throws AbstractException;

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    void removeAll();

    M remove(M model) throws AbstractException;

    M findOneById(String id) throws AbstractException;

    M findOneByIndex(Integer index) throws AbstractException;

    boolean existsById(String id);

}
