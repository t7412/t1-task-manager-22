package ru.t1.chubarov.tm.api.repository;

import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    M add(M model) throws AbstractException;

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    M findOneById(String id) throws AbstractException;

    M findOneByIndex(Integer index) throws AbstractException;

    M remove(M model) throws AbstractException;

    M removeOneById(String id) throws AbstractException;

    M removeOneByIndex(Integer index);

    void removeAll();

    int getSize();

    boolean existsById(String id);

}
