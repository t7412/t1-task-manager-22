package ru.t1.chubarov.tm.api.repository;

import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.model.AbstractUserOwnerModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnerRepository<M extends AbstractUserOwnerModel> extends IRepository<M> {

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator<M> comparator) throws AbstractException;

    M findOneById(String userId, String id) throws AbstractException;

    M findOneByIndex(String userId, Integer index) throws AbstractException;

    M removeOne(String userId, M model) throws AbstractException;

    M removeOneById(String userId, String id) throws AbstractException;

    M removeOneByIndex(String userId, Integer index) throws AbstractException;

    void removeAll(String userId) throws AbstractException;

    M add(String userId, M model) throws AbstractException;

    int getSize(String userId) throws AbstractException;

    boolean existsById(String userId, String id) throws AbstractException;

}
