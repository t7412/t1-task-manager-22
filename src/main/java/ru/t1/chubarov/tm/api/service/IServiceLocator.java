package ru.t1.chubarov.tm.api.service;

public interface IServiceLocator {

    IProjectService getProjectService();

    ITaskService getTaskService();

    IProjectTaskService getProjectTaskService();

    ICommandService getCommandService();

    ILoggerService getLoggerService();

    abstract IUserService getUserService();

    IAuthService getAuthService();

}
