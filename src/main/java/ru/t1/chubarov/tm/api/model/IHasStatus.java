package ru.t1.chubarov.tm.api.model;

import ru.t1.chubarov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
