package ru.t1.chubarov.tm.api.service;

import ru.t1.chubarov.tm.api.repository.IUserOwnerRepository;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.model.AbstractUserOwnerModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnerService<M extends AbstractUserOwnerModel> extends IUserOwnerRepository<M>, IService<M>{

    void removeAll(String userId) throws AbstractException;

    boolean existsById(String userId, String id) throws AbstractException;

    M add(String userId, M model) throws AbstractException;

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator<M> comparator) throws AbstractException;

    M removeOne(String userId, M model) throws AbstractException;

    M findOneById(String userId, String id) throws AbstractException;

    M findOneByIndex(String userId, Integer index) throws AbstractException;

    M removeOneById(String userId, String id) throws AbstractException;

    M removeOneByIndex(String userId, Integer index) throws AbstractException;

    int getSize(String userId) throws AbstractException;

}
